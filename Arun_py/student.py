# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 14:21:13 2022

@author: Arun.Gunda
"""
from person import Person 

class Student(Person):
    def __init__(self, name, last_name):
        super().__init__(name,last_name)
        self.workshop = None
        
    def enroll(self, workshop):
        self.workshop = workshop
    
    def __str__(self): # Dunder methods
        return f"Student {self.name} {self.last_name}"
   


if __name__ == '__main__':
    me = Student('Arun', 'Gunda')
    print(me.workshop)
    print(me.name)
    print(me.last_name)
    print(me.full_name)
    me.enroll('Python workshop')
    print(me.workshop)
    print(me)
    
    him = Person()
    print(him.name)
    print(him.last_name)
    print(him.get_full_name())
    print(him.full_name)